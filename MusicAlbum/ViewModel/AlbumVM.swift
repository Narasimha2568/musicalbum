//
//  AlbumVM.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation

class AlbumVM {
    
    var showFav : Bool = false
    var isSearched : Bool = false
    var albumList : [Album] = []
    var favList : [Album] = []
    var filteredAlbums: [Album] = []

    init(completion: @escaping (_ success: Bool) -> Void) {
        reloadAlbumList(completion: { (success) in
            completion(true)
        })
    }
    
    func searchBartextDidChange(searchText: String) -> [Album] {
        filteredAlbums.removeAll()
        
        if searchText.isEmpty {
            filteredAlbums = []
        } else {
            filteredAlbums = albumList.filter({ (albumObj: Album) -> Bool in
                return (albumObj.album_name?.lowercased().contains(searchText.lowercased()))!
            })
        }
        isSearched = true
        showFav = false
        return filteredAlbums
    }
    
    func clearSearch(completion: @escaping (_ success: Bool) -> Void) {
        isSearched = false
        self.favList = self.albumList.filter{$0.fav == true}
        (self.favList.count > 0) ? (self.showFav = true) : (self.showFav = false)
        completion(true)
    }
    
    func addToFavourites(album: Album,completion: @escaping (_ success: Bool) -> Void) {
        Firebase.shared.updateToFavourites(album: album, completion: { (success) in
            self.reloadAlbumList(completion: { (success) in
                completion(true)
            })
        })
        
    }
    
    func reloadAlbumList(completion: @escaping (_ success: Bool) -> Void) {
        
        Firebase.shared.fetchAlbumList(completion: { (albumList) in
            self.albumList = albumList
            self.favList = self.albumList.filter{$0.fav == true}
            (self.favList.count > 0) ? (self.showFav = true) : (self.showFav = false)
            self.isSearched = false
            completion(true)
        })
    }
}
