//
//  Album.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation

struct Album: Codable {
    
    let image_url : String?
    let album_name : String?
    let artist_name : String?
    var fav : Bool?
    var id : String?
    
}
