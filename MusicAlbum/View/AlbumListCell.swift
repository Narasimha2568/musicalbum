//
//  AlbumListCell.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

protocol AlbumListCellDelegate {
    
    func playBtnAction()
    func pauseBtnAction()
}

class AlbumListCell: UITableViewCell {

     var delegate : AlbumListCellDelegate?
    @IBOutlet weak var albumImage: CustomImageView!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var artistName: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUP(data:Album) {
        albumImage.loadImageUsingUrlString(urlString: data.image_url ?? "")
        albumName.text = data.album_name
        artistName.text = data.artist_name
    }
    
    @IBAction func playBtnAction(_ sender: Any) {
        delegate?.playBtnAction()
    }
    
    @IBAction func pauseBtnAction(_ sender: Any) {
        delegate?.pauseBtnAction()
    }


}
