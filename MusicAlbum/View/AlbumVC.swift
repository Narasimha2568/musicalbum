//
//  ViewController.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit
import AVFoundation

class AlbumVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var musicListTblView: UITableView!
    private var viewModel: AlbumVM! {
        didSet {
            musicListTblView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingNibs()
        viewModel = AlbumVM(completion: { (success) in
            DispatchQueue.main.async {
                self.musicListTblView.reloadData()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.titleView = self.navigationController?.getSearchBarView(to: UIColor.darkText, for: searchBar, searchbar: searchBar)
        self.navigationItem.titleView?.layoutIfNeeded()
    }
    
    // MARK: - loading custom xibs
    func loadingNibs() {
        musicListTblView.register(UINib.init(nibName: "AlbumListCell", bundle: nil), forCellReuseIdentifier: "AlbumListCell")
        musicListTblView.register(UINib.init(nibName: "FavViewCell", bundle: nil), forCellReuseIdentifier: "FavViewCell")
        musicListTblView.rowHeight = UITableView.automaticDimension
        musicListTblView.sectionHeaderHeight = UITableView.automaticDimension
    }
    
}

// MARK: - UISearchBar Delegates
extension AlbumVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchBar.resignFirstResponder()
        viewModel.clearSearch(completion: { (success) in
            DispatchQueue.main.async {
                self.musicListTblView.reloadData()
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        _ = viewModel.searchBartextDidChange(searchText: searchText)
        musicListTblView.reloadData()
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension AlbumVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isSearched {
            return viewModel.filteredAlbums.count
        }
        return viewModel.albumList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if viewModel.showFav {
            let cell = musicListTblView.dequeueReusableCell(withIdentifier: "FavViewCell") as! FavViewCell
            cell.albumVC = self
            cell.favList = viewModel.favList
            return cell
        }
        return nil
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = musicListTblView.dequeueReusableCell(withIdentifier: "AlbumListCell", for: indexPath) as! AlbumListCell
        cell.delegate = self
        if viewModel.isSearched {
            cell.setUP(data: viewModel.filteredAlbums[indexPath.row])
            return cell
        }
        cell.setUP(data: viewModel.albumList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if viewModel.showFav {
            return 150
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.isSearched {
            updateToFavourites(album: viewModel.filteredAlbums[indexPath.row])
        }
        updateToFavourites(album: viewModel.albumList[indexPath.row])
    }
    
    func updateToFavourites(album:Album) {
        viewModel.addToFavourites(album: album, completion: { (success) in
            DispatchQueue.main.async {
                self.musicListTblView.reloadData()
                self.scrollMusicList(album: album, addtoFav: true)
            }
        })

    }
   
    func scrollMusicList(album:Album, addtoFav:Bool) {
        var indexVal = 0
        if !addtoFav {
            indexVal = Int(album.id!)!
            if indexVal == viewModel.albumList.count {
                indexVal = indexVal - 1
            }
        }
        let indexPath = IndexPath(row: indexVal, section: 0)
        self.musicListTblView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}

// MARK: - AlbumListCellDelegate
extension AlbumVC : AlbumListCellDelegate {
    
    func playBtnAction() {
        AudioPlayer.shared.play()
    }
    
    func pauseBtnAction() {
         AudioPlayer.shared.pause()
    }
    
}
