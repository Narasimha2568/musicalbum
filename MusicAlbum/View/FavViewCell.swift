//
//  FavViewCell.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class FavViewCell: UITableViewCell {
    @IBOutlet weak var favListColView: UICollectionView!
    var favList : [Album]! {
        didSet {
            favListColView.reloadData()
        }
    }
    var albumVC:AlbumVC?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadingNibs()
    }

    // MARK: - loading custom xibs
    func loadingNibs() {
        favListColView.register(UINib.init(nibName: "FavListCell", bundle: nil), forCellWithReuseIdentifier: "FavListCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension FavViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = favListColView.dequeueReusableCell(withReuseIdentifier: "FavListCell", for: indexPath) as! FavListCell
        
        cell.setUP(data: favList[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        albumVC?.scrollMusicList(album: favList[indexPath.item], addtoFav: false)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = 120.0
        let height = 120.0
        return CGSize.init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
}
