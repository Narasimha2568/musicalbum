//
//  FavListCell.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import UIKit

class FavListCell: UICollectionViewCell {

    @IBOutlet weak var albumImage: CustomImageView!
    @IBOutlet weak var albumName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        albumImage.roundview(cornerRadius: 4.0, borderWidth: 0.5, borderColor: #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5).cgColor)
    }

    func setUP(data:Album) {
        albumImage.loadImageUsingUrlString(urlString: data.image_url ?? "")
        albumName.text = data.album_name
    }
}
