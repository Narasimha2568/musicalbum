//
//  AudioPlayer.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer {
    
    static let shared:AudioPlayer = AudioPlayer()
    var player : AVPlayer?
    
    init() {
        guard let url = URL.init(string: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-6.mp3") else { return }
        let playerItem = AVPlayerItem.init(url: url)
        player = AVPlayer.init(playerItem: playerItem)
    }
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
}
