//
//  Firebase.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 23/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import FirebaseDatabase
import CodableFirebase

class Firebase {
    static let shared:Firebase = Firebase()
    var ref: DatabaseReference?
    var refHandle : DatabaseHandle?
    
    func fetchAlbumList(completion: @escaping (_ result: [Album]) ->Void) {
        
        ref = Database.database().reference()
        
        var albumList = [Album]()
        
        refHandle = self.ref!.child("List").observe(.childAdded) { (snapshot) in
            
            guard let value = snapshot.value else { return }
            do {
                let model = try FirebaseDecoder().decode(Album.self, from: value)
                debugPrint(model)
                albumList.append(model)
            } catch let error {
                debugPrint(error)
            }
            debugPrint(albumList)
            completion(albumList)
        }
    }
    
    func updateToFavourites(album:Album, completion: @escaping (_ success: Bool) -> Void) {
        
        var favalbum = album
       (album.fav == true) ? (favalbum.fav = false) : (favalbum.fav = true)
        ref = Database.database().reference()
        do {
            let model = try FirebaseEncoder().encode(favalbum)
            self.ref!.child("List").child("\(album.id!)").setValue(model)
            completion(true)
        } catch let error {
            debugPrint(error)
        }
        
    }
    
}
