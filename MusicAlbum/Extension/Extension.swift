//
//  Extension.swift
//  MusicAlbum
//
//  Created by Narasimha Poluparthi on 22/06/19.
//  Copyright © 2019 Narasimha Poluparthi. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UINavigationController
extension UINavigationController {
    
    func getSearchBarView(to color: UIColor, for view: UIView, searchbar: UISearchBar) -> UISearchBar {
        
        searchbar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        if view is UITextField {
            view.tintColor = color
        }
        for subview in view.subviews {
            _ = getSearchBarView(to: color, for: subview, searchbar: searchbar)
        }
        navigationItem.titleView = searchbar
        self.navigationItem.hidesBackButton = true
        return searchbar
    }
}

// MARK: - UIImageView
extension UIImageView {

    func roundview(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: CGColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }
}
